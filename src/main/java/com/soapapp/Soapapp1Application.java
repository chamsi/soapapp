package com.soapapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Soapapp1Application {

	public static void main(String[] args) {
		SpringApplication.run(Soapapp1Application.class, args);
	}

}
