package com.soapapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.soapapp.model.GetCountryRequest;
import com.soapapp.model.GetCountryResponse;
import com.soapapp.repo.CountryRepo;

@Endpoint
public class CountryEndpoint {

	private static final String NAMESPACE_URI = "http://www.soapapp.com/model";
	private CountryRepo countryRepository;

	@Autowired
	public CountryEndpoint(CountryRepo countryRepository) {
		this.countryRepository = countryRepository;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCountryRequest")
	@ResponsePayload
	public GetCountryResponse getCountry(@RequestPayload GetCountryRequest request) {
		GetCountryResponse response = new GetCountryResponse();
		response.setCountry(countryRepository.findCountry(request.getName()));
		return response;
	}
}